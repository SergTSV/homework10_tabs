// Посмотрев информацию о данных которые присутствуют в элементах решил
// сделать код используя только эти данные, ничего не добавляя типа "data-name"
// по моему мнению так проще, компактней и быстрее ....



const tabs = document.querySelector('.tabs');
const tabsContent = document.querySelector('.tabs-content');
const tabsTitles = document.querySelectorAll('.tabs-title');
const tabContentNode = Array.from(tabsContent.childNodes);


for (const tabsTitle of tabsTitles) {
    if (tabsTitle.classList.contains('active')) {
        tabContentNode.forEach((el => {
            if (el.nodeType === 8) {
                if (tabsTitle.textContent.trim() === el.textContent.trim()) {
                    el.nextElementSibling.style.display = 'flex';
                } else {
                    el.nextElementSibling.style.display = 'none';
                }
            }
        }))
    }
}

tabs.addEventListener('click', e => {
    for (const tab of tabsTitles) {
        tab.classList.remove('active')
    }
    e.target.classList.add('active')

    tabContentNode.forEach((el => {
        if (el.nodeType === 8) {
            if (e.target.textContent.trim() === el.textContent.trim()) {
                el.nextElementSibling.style.display = 'flex';
            } else {
                el.nextElementSibling.style.display = 'none';
            }
        }
    }));
})
